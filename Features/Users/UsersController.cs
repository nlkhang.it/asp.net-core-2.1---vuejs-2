﻿using AspNetCoreVueJs.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace AspNetCoreVueJs.Features.Users
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly EcommerceContext _dbContext;

        public UsersController(EcommerceContext db)
        {
            _dbContext = db;
        }
        [
            HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _dbContext.Users.ToListAsync());
        }
    }
}