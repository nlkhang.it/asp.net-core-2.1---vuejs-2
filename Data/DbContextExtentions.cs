﻿using AspNetCoreVueJs.Data.Entities;
using Microsoft.AspNetCore.Identity;

namespace AspNetCoreVueJs.Data
{
    public static class DbContextExtensions
    {
        public static UserManager<AppUser> UserManager { get; set; }

        public static void EnsureSeeded(this EcommerceContext context)
        {
            const string email = "khangnguyen@gmail.com";

            if (UserManager.FindByEmailAsync(email).GetAwaiter().GetResult() == null)
            {
                var user = new AppUser
                {
                    FirstName = "Stu",
                    LastName = "Ratcliffe",
                    UserName = email,
                    Email = email,
                    EmailConfirmed = true,
                    LockoutEnabled = false
                };

                UserManager.CreateAsync(user, "P@ssw0rd").GetAwaiter().GetResult();
            }
        }
    }
}
